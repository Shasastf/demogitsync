---
title: ' QQOQCCP de Chocolatey'
media_order: chocolateyLogo.png
---

![](chocolateyLogo.png)
Ces deux pages vont avoir pour objectif de fournir plus d'information sur ce que c'est Chocolatey et sur comment l'utiliser.
## Chocolatey c'est qui?
Chocolatey est ce qu'on appele un gestionnaire de paquets ou en d'autre terme un software management. C'est un gestionnaire de paquets plutôt spécial puisque pour utiliser Chocolatey il faut être être dans l'invite de commandes (Cmd) et c'est également dans l'invite de comandes que Chocolatey va être installé. 

## Chocolatey c'est quoi?
Comme dit auparavant, Chocolatey est un gestionnaire de paquest qui va favoriser la simplicité. Il va donc permettre une installation, suppression et mise à jour de façon très simple et rapide de logiciel et d'application sur Windows. Dit comme ça, ça peut paraître un peu inutile, mais en réalité c'est extrémement pratique et utile. Voici plusieurs raison:
* Avec une simple ligne et la touche enter, tu peux installer, mêttre à jour et supprimer des logiciels.
* Sécuritaire, car il n'y a beaucoup moins de risque de télécharger un logiciel malveillant.

## Chocolatey c'est où?

#### Références
1.Logo Chocolatey [](https://chocolatey.org/)